FROM alpine:latest as build

RUN apk --no-cache add wget \
                       npm

RUN mkdir /crontab-ui /extract \
 && export VERSION=$(wget -qO- https://api.github.com/repos/alseambusher/crontab-ui/releases/latest | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/') \
 && wget -qO- https://github.com/alseambusher/crontab-ui/archive/refs/tags/${VERSION}.tar.gz | tar xvz -C /extract \
 && mv /extract/crontab-ui*/* /crontab-ui/ \
 && cp /crontab-ui/supervisord.conf /etc/supervisord.conf \
 && rm -rf /crontab-ui/Dockerfile \
           /crontab-ui/docker-compose.yml \
           /crontab-ui/README \
           /crontab-ui/README.md \
           /crontab-ui/tests

RUN cd /crontab-ui \
 && npm ci

#### docker:dind based on Alpine Linux image
FROM docker:dind

LABEL image.name="epicsoft_crontab_ui" \
      image.description="Docker image for Crontab UI" \
      maintainer="epicsoft.de" \
      maintainer.name="Alexander Schwarz <schwarz@epicsoft.de>" \
      maintainer.copyright="Copyright 2022 epicsoft.de / Alexander Schwarz" \
      license="MIT"

ENV CRON_PATH /etc/crontabs
ENV HOST 0.0.0.0
ENV PORT 8000
ENV CRON_IN_DOCKER true

WORKDIR /crontab-ui

RUN apk --no-cache add nodejs \
                       supervisor \
                       wget \
                       curl \
                       tzdata \
                       bash

RUN ln -s /usr/bin/node /usr/local/bin/node

RUN mkdir -p /crontab-ui /etc/crontabs \
 && touch /etc/crontabs/root \
 && chmod +x /etc/crontabs/root

COPY --from=build /crontab-ui/supervisord.conf /etc/supervisord.conf
COPY --from=build /crontab-ui /crontab-ui

EXPOSE 8000

CMD ["supervisord", "-c", "/etc/supervisord.conf"]
