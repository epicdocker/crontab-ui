<!-- vscode-markdown-toc -->
* 1. [Versions](#Versions)
* 2. [Requirements](#Requirements)
* 3. [Environment variables](#Environmentvariables)
* 4. [Volumes / Mounts](#VolumesMounts)
* 5. [Examples](#Examples)
* 6. [Project-License](#Project-License)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

# Crontab UI

Docker image for [Crontab UI](https://github.com/alseambusher/crontab-ui) based on [Docker](https://hub.docker.com/_/docker) image with DinD.

When running the cronjobs Docker (DinD) can be used if started with the mount `/var/run/docker.sock`, see examples.


##  1. <a name='Versions'></a>Versions

Of this project, there is always only one version `latest` and so that the version stays up to date, it is rebuilt weekly.

`latest` [Dockerfile](https://gitlab.com/epicdocker/crontab-ui/-/blob/master/Dockerfile)


##  2. <a name='Requirements'></a>Requirements

- Docker installed - https://www.docker.com/get-started


##  3. <a name='Environmentvariables'></a>Environment variables

- `BASIC_AUTH_USER` and `BASIC_AUTH_PWD` enable basic auth for UI (default: *None*)
- `CRON_IN_DOCKER` Runs the cronjobs in the container (default: `true`)


##  4. <a name='VolumesMounts'></a>Volumes / Mounts

- `/crontab-ui/crontabs/` Storage path to Crontab-, Environment-DB and backups
- `/var/run/docker.sock` Connection to Docker Host to execute Docker commands inside the container.


##  5. <a name='Examples'></a>Examples

```bash
docker run -d --name crontab-ui \
  --restart unless-stopped \
  -v /host/path/crontabs/:/crontab-ui/crontabs/:rw \
  -v /var/run/docker.sock:/var/run/docker.sock:rw \
  -p 8000:8000 \
    registry.gitlab.com/epicdocker/crontab-ui
```


##  6. <a name='Project-License'></a>Project-License

MIT License see [LICENSE](LICENSE)
